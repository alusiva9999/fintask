import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateuserComponent } from './createuser/createuser.component';
import { UserslistComponent } from './userslist/userslist.component';
import { TaskComponent } from './task/task.component';


const routes: Routes = [{ path: '', redirectTo: '/createuser', pathMatch: 'full' },
{ path: 'createuser', component: CreateuserComponent },
{path : 'user', component: UserslistComponent},
{path : 'task', component : TaskComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
