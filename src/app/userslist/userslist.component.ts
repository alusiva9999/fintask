import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-userslist',
  templateUrl: './userslist.component.html',
  styleUrls: ['./userslist.component.scss']
})
export class UserslistComponent implements OnInit {
  @Input() userdetails: any;
  objs: any;
  @Output() value = new EventEmitter<any>();
  // @Input() userslist: any;


  constructor() {
    console.log('the input list is', this.userdetails);
  }
  ngOnInit() {
    console.log('the input list is', this.userdetails);

  }
  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit() {
    console.log('the input list1 is', this.userdetails);
  }
  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterContentInit() {
    console.log('the input list2 is', this.userdetails);
  }

  save() {
    this.objs = {
      name: 'vijay',
      age: 17
    };

    this.value.emit(this.objs);
  }


}
