import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateuserComponent } from './createuser/createuser.component';
import { FormsModule } from '@angular/forms';
import { UserslistComponent } from './userslist/userslist.component';

import { HttpModule } from '@angular/http';
import { UsersService } from './users.service';
import { TaskComponent } from './task/task.component';


@NgModule({
  declarations: [
    AppComponent,
    CreateuserComponent,
    UserslistComponent,
    TaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule

  ],
  providers: [ UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
