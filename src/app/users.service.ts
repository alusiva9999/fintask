import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: Http) { }
  createusers(data1) {

    console.log('the datas', data1);
    const headers1 = new Headers();
    headers1.append('Content-Type', 'application/json; charset=utf-8');
    headers1.append('Allow-Control-Allow-Origin', '*');
    // https://sandbox786.com/ng-api/createUser
    return this.http.post('https://sandbox786.com/ng-api/createUser', data1, { headers: headers1 });

    // return this.http.post('https://sandbox786.com/ng-api/createUser', data1, { headers: headers1 }).map((res) => {
    //   return res.json();
    // });
  }

  getuserslist(data) {
    // const headers1 = new Headers();
    // headers1.append('Content-Type', 'application/json; charset=utf-8');
    // headers1.append('Allow-Control-Allow-Origin',  '*');
    // let data1=
    // return this.http.post('https://sandbox786.com/ng-api/getUsers', JSON.stringify(data ), {
    //   headers: new Headers({
    //     'Content-Type': 'application/json'
    //   })
    // });
    return this.http.post('https://sandbox786.com/ng-api/getUsers', JSON.stringify(data) );
  }

  updateuser(data) {
    // https://sandbox786.com/ng-api/updateUser
    return this.http.post('https://sandbox786.com/ng-api/updateUser', data);
  }


}
