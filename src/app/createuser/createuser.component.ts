import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { Router } from '@angular/router';




@Component({
  selector: 'app-createuser',
  templateUrl: './createuser.component.html',
  styleUrls: ['./createuser.component.scss']
})
export class CreateuserComponent implements OnInit {
  userdetails: any;
  userslist: any = [];
  editusers = false;
  update = false;
  save = false;
  constructor(public usersservice: UsersService, private router: Router) {

    this.getusers();
    this.userdetails = {
      auth_code: 'test298',
      user_name: null,
      user_email: null,
      user_age: null,
      user_phone: null,
      user_id: null
    };


  }

  ngOnInit() {
  }

  createuser(userdetails) {

    this.router.navigate(['/user']);
    console.log('userdteails is', this.userdetails);
    this.usersservice.createusers(userdetails).subscribe((res) => {

      console.log('server res', res.json());
      this.getusers();
      this.userdetails = '';
      this.save = true;
      setTimeout(() => {
        this.save = false;
      }, 2000);
    });

  }

  getusers() {
    this.usersservice.getuserslist({ auth_code: 'test298' }).subscribe((res) => {
      console.log('get call res', res.json());
      const data: any = res.json();
      this.userslist = data.responce;
    });
  }

  updateuser(data) {
    console.log('the edit is', data);
    this.usersservice.updateuser(data).subscribe((res) => {
      console.log('server res', res);
      this.getusers();
      this.update = true;
      setTimeout(() => {
        this.update = false;
      }, 2000);
    });
    this.editusers = false;
    this.userdetails = '';
  }

  edituser(user) {
    this.editusers = true;
    this.userdetails = {
      auth_code: 'test298',
      user_name: user.user_name,
      user_email: user.user_email,
      user_age: user.user_age,
      user_phone: user.user_phone,
      user_id: user.user_id
    };
  }

}
